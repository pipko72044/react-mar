import React from "react";
import s from './../HeaderBlock/HeaderBlock.module.css'

const ContentBlock = ({heading, descr}) => {
    return (
    <div className={s.content}>
        <div>
            <h2>{heading}</h2>
            <p> {descr} </p>
        </div>
    </div>
    )
};

export default ContentBlock