import React from 'react';
import HeaderBlock from "./components/HeaderBlock";
import ContentBlock from "./components/ContentBlock";
import FooterBlock from "./components/FooterBlock";

const App = () => {
    return (
        <>
            <HeaderBlock
                title="Учите слова онлайн"
                descr="Используйте карточки для запоминания и пополняйте активный словарный запас"
            />
            <HeaderBlock
                title="Нам нравится это"
                hideBackground
            />
            <ContentBlock
                heading="Заголовок"
                descr=" абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста
                 абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста
                  абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста
                   абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста абзац текста"
            />
            <FooterBlock
                cop="мой копирайт =\"
                vk="#"
                inst="#"
            />
        </>
    )
};


export default App;
